# Version: 0.0.1
FROM python
LABEL MAINTAINER="JonDoue"
# ENV VAR1=0 VAR2=0
RUN apt-get update
    # apt-get git 
    # apk add --no-cache python3
RUN git clone https://gitlab.com/AriohBy/docker /data/app
WORKDIR /data/app
# RUN python3 script1.py $var1 $var2
ENTRYPOINT ["python", "script1.py"]
CMD ["0"]
# VOLUME [ "/data/app/res" ]
# CMD ["python3", "-m", "script1"]
# RUN python3 -m script1 $VAR1 $VAR2

